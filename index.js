import React from 'react';
import { Provider } from 'react-redux';

import { AppRegistry } from 'react-native';
import App from './src/App';
import { name as weatherApp } from './app.json';

import store from './src/services/redux/store';

const RNRedux = () => (
	<Provider store={store}>
		<App />
	</Provider>
);

AppRegistry.registerComponent(weatherApp, () => RNRedux);
