module.exports = {
	bracketSpacing: true,
	jsxBracketSameLine: true,
	singleQuote: true,
	trailingComma: 'all',
	rintWidth: 100
	// arrowParens: 'avoid',
};
