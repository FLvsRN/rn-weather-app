import React, { Component } from 'react';

import { View, Text, FlatList, StyleSheet } from 'react-native';
import LottieView from 'lottie-react-native';

import { WEEKDAYS, IMAGES } from '../../services/constants/';

class WeekForecast extends Component {
	render() {
		const { weather } = this.props;

		const renderItem = ({ item, index }) => (
			<View style={styles.weatherContainer}>
				<Text style={styles.day}>{index == 0 ? 'Tomorrow' : WEEKDAYS[new Date(item.date).getDay()]}</Text>
				<LottieView source={IMAGES[item.state]} autoPlay loop autosize resizeMode="cover" style={styles.icon} />
				<Text style={styles.temperature}> {Math.round(item.temperature)}℃</Text>
			</View>
		);

		return (
			<View style={styles.weekForecastContainer}>
				<FlatList horizontal data={weather} renderItem={renderItem} />
			</View>
		);
	}
}

const styles = StyleSheet.create({
	weekForecastContainer: {
		flex: 2,
		paddingHorizontal: 5
	},
	weatherContainer: {
		backgroundColor: 'rgba(255, 255, 255, 0.5)',
		margin: 5,
		padding: 8,
		alignItems: 'center',
		flexDirection: 'column',
		justifyContent: 'space-evenly',
		borderRadius: 5
	},
	day: {
		fontWeight: 'bold'
	},
	icon: {
		width: 65,
		height: 65
	},
	temperature: {
		fontWeight: '400'
	}
});

export default WeekForecast;
