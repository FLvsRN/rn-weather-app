import React, { Component } from 'react';

import { Text, View, Dimensions, StyleSheet } from 'react-native';
import LottieView from 'lottie-react-native';

import { IMAGES } from '../../services/constants';

class CurrentWeather extends Component {
	render() {
		const { weather } = this.props;
		return (
			<View style={styles.currentWeatherContainer}>
				<Text style={styles.location}>{weather.location}</Text>
				<Text style={styles.weatherState}> {weather.stateName} </Text>
				<View style={styles.weatherContainer}>
					<LottieView
						source={IMAGES[weather.state]}
						autoPlay
						loop
						autosize
						resizeMode="center"
						style={styles.icon}
					/>
					<View>
						<Text style={styles.temperature}>{weather.temperature}℃</Text>
						<View style={styles.temperaturesContainer}>
							<Text style={styles.miniTemperature}>H:{weather.maxTemperature}°</Text>
							<Text style={styles.miniTemperature}>L:{weather.minTemperature}°</Text>
						</View>
					</View>
				</View>
			</View>
		);
	}
}

styles = StyleSheet.create({
	currentWeatherContainer: {
		flex: 5,
		alignItems: 'center',
		justifyContent: 'center',
		flexDirection: 'column'
	},
	weatherContainer: {
		flexDirection: 'row',
		alignItems: 'center',
		margin: 15
	},
	icon: {
		width: Dimensions.get('window').width * 0.5,
		height: Dimensions.get('window').width * 0.5
	},
	temperature: {
		fontSize: 54,
		fontWeight: '600',
		margin: 10,
		paddingHorizontal: 10
	},
	weatherState: {
		fontSize: 16,
		fontWeight: '200',
		margin: 0
	},
	location: {
		fontSize: 42,
		fontWeight: '300',
		margin: 5
	},
	temperaturesContainer: {
		flexDirection: 'row',
		justifyContent: 'space-evenly',
		marginHorizontal: 10
	},
	miniTemperature: {
		fontWeight: '300'
	}
});

export default CurrentWeather;
