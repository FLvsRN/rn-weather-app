import React, { Component } from 'react';

import { View } from 'react-native';
import { IconButton, Colors } from 'react-native-paper';

class BottonButton extends Component {
	render() {
		return (
			<View style={{ flexDirection: 'row', justifyContent: 'flex-end' }}>
				<IconButton
					icon="menu"
					color={Colors.white}
					size={20}
					onPress={() => this.props.navigation.navigate('City')}
				/>
			</View>
		);
	}
}

export default BottonButton;
