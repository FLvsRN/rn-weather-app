import React, { Component } from 'react';

import { View, Text, FlatList, StyleSheet } from 'react-native';

class WeatherDetails extends Component {
	render() {
		const { weather } = this.props;

		return (
			<View style={styles.weatherDetailsContainer}>
				<FlatList
					horizontal
					data={[
						{ key: 'Air pressure', value: weather.airPressure, unit: ' hPa' },
						{ key: 'Humidity', value: weather.humidity, unit: '%' },
						{ key: 'Wind ', value: weather.windSpeed, unit: ' mph' },
						{ key: 'Wind direction', value: weather.windDirection, unit: '' },
						{ key: 'Visibility', value: weather.visibility, unit: ' miles' }
					]}
					renderItem={({ item }) => (
						<View style={styles.detailContainer}>
							<Text style={styles.detailName}>{item.key}</Text>
							<Text style={styles.detailValue}>
								{item.value}
								{item.unit}
							</Text>
						</View>
					)}
				/>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	weatherDetailsContainer: {
		flex: 2,
		alignItems: 'center',
		justifyContent: 'center',
		flexDirection: 'row',
		marginHorizontal: 5
	},
	detailContainer: {
		backgroundColor: 'white',
		width: 150,
		margin: 5,
		alignItems: 'flex-start',
		justifyContent: 'space-evenly',
		padding: 10,
		borderRadius: 5
	},
	detailName: {
		fontSize: 18,
		opacity: 0.7,
		margin: 5,
		fontWeight: '300'
	},
	detailValue: {
		fontSize: 28,
		margin: 5,
		fontWeight: '400'
	}
});

export default WeatherDetails;
