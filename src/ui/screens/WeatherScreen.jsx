import React, { Component } from 'react';

import { SafeAreaView, Text, View, StyleSheet, RefreshControl, ScrollView, StatusBar } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { Divider, Button } from 'react-native-paper';

import { CurrentWeather, WeekForecast, WeatherDetails, BottomButton } from '../components';

import { fetchWeather } from '../../services/redux/actions/weather';
import { connect } from 'react-redux';

class WeatherScreen extends Component {
	fetchWeather = async () => {
		await this.props.fetchWeather(this.props.weather[0].location);
	};

	render() {
		const { isFetching, weather } = this.props;

		return (
			<LinearGradient
				colors={[ '#acb6e5', '#86fde8' ]}
				start={{ x: 0, y: 1 }}
				end={{ x: 1.3, y: 0 }}
				style={{ flex: 1 }}
			>
				<SafeAreaView style={{ flex: 1, flexDirection: 'column' }}>
					<StatusBar translucent backgroundColor="transparent" />
					{weather ? (
						<ScrollView
							contentContainerStyle={{ flexGrow: 1 }}
							refreshControl={<RefreshControl refreshing={isFetching} onRefresh={this.fetchWeather} />}
						>
							<CurrentWeather weather={weather[0]} />
							<WeekForecast
								weather={weather.slice(weather.length - (weather.length - 1), weather.length)}
							/>
							<WeatherDetails weather={weather[0]} />
							<View>
								<Divider style={styles.divider} />
								<BottomButton navigation={this.props.navigation} />
							</View>
						</ScrollView>
					) : (
						<View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
							<Text style={styles.message}>Please Select a Location</Text>
							<Button
								mode="contained"
								onPress={() => this.props.navigation.navigate('City')}
								style={styles.button}
							>
								Search
							</Button>
						</View>
					)}
				</SafeAreaView>
			</LinearGradient>
		);
	}
}

const styles = StyleSheet.create({
	divider: {
		backgroundColor: 'white'
	},
	button: {
		margin: 10,
		borderRadius: 10,
		width: '60%',
		backgroundColor: '#9CBAFF'
	},

	message: {
		margin: 25,
		fontWeight: '400',
		fontSize: 24,
		color: 'white'
	}
});

const mapActionToProps = {
	fetchWeather
};

const mapStateToProps = (state) => ({ ...state.weather });

export default connect(mapStateToProps, mapActionToProps)(WeatherScreen);
