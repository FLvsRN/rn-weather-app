import React, { Component } from 'react';

import { View, StyleSheet, TextInput, Alert, Text } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import { Button } from 'react-native-paper';

import { fetchWeather } from '../../services/redux/actions/weather';
import { connect } from 'react-redux';

import _ from 'underscore';

class CitySelectScreen extends Component {
	constructor(props) {
		super(props);
		this.state = { text: '' };
	}

	fetchWeather = async () => {
		await this.props.fetchWeather(this.state.text);

		this.props.error ? this.createAlert() : this.props.navigation.pop();
	};

	createAlert = (error) => {
		Alert.alert('Error', _.isEmpty(error) ? 'Something went wrong!' : { error }, [
			{ text: 'OK', onPress: () => this.setState({ text: '' }) }
		]);
	};

	render() {
		const { isFetching } = this.props;

		return (
			<View style={styles.mainContainer}>
				{isFetching ? (
					<Spinner visible={isFetching} textContent={'Loading...'} textStyle={styles.spinnerTextStyle} />
				) : (
					<View style={{ alignItems: 'center' }}>
						<TextInput
							style={styles.textInput}
							onChangeText={(text) => this.setState({ text })}
							value={this.state.text}
							placeholder="Enter city name..."
							clearButtonMode="always"
						/>
						<Button
							mode="contained"
							onPress={this.fetchWeather}
							style={styles.button}
							disabled={this.state.text ? false : true}
							color={'#9CBAFF'}
							dark
						>
							Search
						</Button>
					</View>
				)}
				<Text style={styles.label}>React Native</Text>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	mainContainer: { flex: 1 },
	spinnerTextStyle: {
		color: 'white'
	},
	textInput: {
		height: 40,
		width: '90%',
		borderWidth: 1,
		borderColor: 'gray',
		paddingLeft: 20,
		margin: 10,
		borderRadius: 20
	},
	button: {
		margin: 10,
		borderRadius: 10,
		width: '70%'
	},
	label: {
		position: 'absolute',
		bottom: 0,
		margin: 10,
		fontWeight: '300'
	}
});

const mapActionToProps = {
	fetchWeather
};

const mapStateToProps = (state) => ({ ...state.weather });

export default connect(mapStateToProps, mapActionToProps)(CitySelectScreen);
