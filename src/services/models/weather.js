export class WeatherState {
	static snow = 'snow';
	static sleet = 'sleet';
	static hail = 'hail';
	static thunderstorm = 'thunderstorm';
	static heavyRain = 'heavyRain';
	static lightRain = 'lightRain';
	static showers = 'showers';
	static heavyCloud = 'heavyCloud';
	static lightCloud = 'lightCloud';
	static clear = 'clear';
	static unkown = 'unkown';
}

export class Weather {
	constructor(
		id,
		state,
		stateName,
		location,
		date,
		temperature,
		minTemperature,
		maxTemperature,
		humidity,
		windSpeed,
		windDeirection,
		airPressure,
		visibility
	) {
		this.id = id;
		this.state = state;
		this.stateName = stateName;
		this.location = location;
		this.date = date;
		this.temperature = temperature;
		this.minTemperature = minTemperature;
		this.maxTemperature = maxTemperature;
		this.humidity = humidity;
		this.windSpeed = windSpeed;
		this.windDeirection = windDeirection;
		this.airPressure = airPressure;
		this.visibility = visibility;
	}
	from(json = {}) {
		const res = {
			id: json.id,
			state: this.mapAbbrToState(json.weather_state_abbr),
			stateName: json.weather_state_name,
			location: json.title,
			date: json.applicable_date,
			temperature: Math.round(json.the_temp),
			minTemperature: Math.round(json.min_temp),
			maxTemperature: Math.round(json.max_temp),
			humidity: Math.round(json.humidity),
			windSpeed: Math.round(json.wind_speed),
			windDirection: json.wind_direction_compass,
			airPressure: Math.round(json.air_pressure),
			visibility: Math.round(json.visibility)
		};
		return Object.assign(this, res);
	}

	mapAbbrToState(input) {
		let state;
		switch (input) {
			case 'sn':
				state = WeatherState.snow;
				break;
			case 'sl':
				state = WeatherState.sleet;
				break;
			case 'h':
				state = WeatherState.hail;
				break;
			case 't':
				state = WeatherState.thunderstorm;
				break;
			case 'hr':
				state = WeatherState.heavyRain;
				break;
			case 'lr':
				state = WeatherState.lightRain;
				break;
			case 's':
				state = WeatherState.showers;
				break;
			case 'hc':
				state = WeatherState.heavyCloud;
				break;
			case 'lc':
				state = WeatherState.lightCloud;
				break;
			case 'c':
				state = WeatherState.clear;
				break;
			default:
				state = WeatherState.unkown;
				break;
		}
		return state;
	}
}

export default { Weather, WeatherState };
