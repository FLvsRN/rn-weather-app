export const IMAGES = {
	snow: require('../../../assets/lottie/moderate-snow.json'),
	sleet: require('../../../assets/lottie/sleet.json'),
	hail: require('../../../assets/lottie/hail.json'),
	thunderstorm: require('../../../assets/lottie/thunderstorm.json'),
	heavyRain: require('../../../assets/lottie/heavy-rain.json'),
	lightRain: require('../../../assets/lottie/light-rain.json'),
	showers: require('../../../assets/lottie/showers.json'),
	heavyCloud: require('../../../assets/lottie/overcast.json'),
	lightCloud: require('../../../assets/lottie/light-cloud.json'),
	clear: require('../../../assets/lottie/sunny.json')
};
