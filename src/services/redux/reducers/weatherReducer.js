import { WEATHER } from '../actions/weather';

const initialState = {
	weather: null,
	isFetching: false,
	error: null
};

const weatherReducer = (state = initialState, action) => {
	switch (action.type) {
		case WEATHER.START_FETCH:
			return { ...state, isFetching: true };
		case WEATHER.FINISH_FETCH:
			return { isFetching: false, weather: action.payload, error: null };
		case WEATHER.FAIL_FETCH:
			return { ...state, isFetching: false, error: action.payload };
		default:
			return state;
	}
};

export default weatherReducer;
