import { getLocationId, getWeather } from '../../api/weather_api';

export const WEATHER = {
	START_FETCH: 'WEATHER_START_FETCH',
	FINISH_FETCH: 'WEATHER_FINISH_FETCH',
	FAIL_FETCH: 'WEATHER_FAIL_FETCH'
};

export const startFetchWeather = () => ({
	type: WEATHER.START_FETCH
});

export const finishFetchWeather = (weather) => ({
	type: WEATHER.FINISH_FETCH,
	payload: weather
});

export const failFetchWeather = (error) => ({
	type: WEATHER.FAIL_FETCH,
	payload: error
});

export const fetchWeather = (city) => async (dispatch) => {
	dispatch(startFetchWeather());

	try {
		const locationId = await getLocationId(city);
		try {
			const weather = await getWeather(locationId);
			dispatch(finishFetchWeather(weather));
		} catch (error) {
			dispatch(failFetchWeather(error));
		}
	} catch (error) {
		dispatch(failFetchWeather(error));
	}
};
