const baseUrl = 'https://www.metaweather.com/api/location/';

import { Weather } from '../models/weather';

export const getLocationId = async (city) => {
	const response = await fetch(baseUrl + `search/?query=${city}`);

	if (!response.ok) {
		const resData = await response.json();
		throw new Error(resData.message);
	}

	const resData = await response.json();

	const res = resData[0];

	return res.woeid;
};

export const getWeather = async (woeid) => {
	const response = await fetch(baseUrl + woeid);

	let { title, consolidated_weather } = await response.json();
	let forecast = consolidated_weather.map((weather) => new Weather().from({ ...weather, title: title }));

	return forecast;
};
