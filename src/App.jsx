import 'react-native-gesture-handler';
import React, { Component } from 'react';

import { WeatherScreen, CitySelectScreen } from './ui/screens/';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

class App extends Component {
	render() {
		return (
			<NavigationContainer>
				<Stack.Navigator
					screenOptions={{
						cardStyle: { backgroundColor: 'transparent' },
						cardOverlayEnabled: true
					}}
				>
					<Stack.Screen name="Weather" component={WeatherScreen} options={{ header: () => null }} />
					<Stack.Screen
						name="City"
						component={CitySelectScreen}
						options={{
							headerBackTitle: 'Back',
							headerStyle: {
								backgroundColor: '#48D3FF'
							},
							headerTintColor: '#fff'
						}}
					/>
				</Stack.Navigator>
			</NavigationContainer>
		);
	}
}
export default App;
