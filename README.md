# Weather app - React Native

Simple weather mobile app.

## Built with 

- React Native
- Redux
- metaweather.com API
- Lottie

## Screenshots

### iOS

###### Home screen

<img src="/screenshots/ios/Simulator Screen Shot - iPhone 8 - 2021-06-21 at 23.00.26.png" height="200">

###### Weather screen

<img src="/screenshots/ios/Simulator Screen Shot - iPhone 8 - 2021-06-21 at 23.00.47.png" height="200">

###### Location change screen

<img src="/screenshots/ios/Simulator Screen Shot - iPhone 8 - 2021-06-21 at 23.00.30.png" height="200">
<img src="/screenshots/ios/Simulator Screen Shot - iPhone 8 - 2021-06-21 at 23.00.37.png" height="200">

###### Refresh view

<img src="/screenshots/ios/Simulator Screen Shot - iPhone 8 - 2021-06-21 at 23.00.49.png" height="200">

### Android

###### Home screen

<img src="/screenshots/android/Screenshot_1624309438.png" height="200">

###### Weather screen

<img src="/screenshots/android/Screenshot_1624309452.png" height="200">

###### Location change screen

<img src="/screenshots/android/Screenshot_1624309441.png" height="200">
<img src="/screenshots/android/Screenshot_1624309447.png" height="200">

###### Refresh view

<img src="/screenshots/android/Screenshot_1624309462.png" height="200">

## License
[![License: CC BY-SA 4.0](https://img.shields.io/badge/License-CC%20BY--SA%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by-sa/4.0/)

This work is licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).

[![License: CC BY-SA 4.0](https://licensebuttons.net/l/by-sa/4.0/80x15.png)](https://creativecommons.org/licenses/by-sa/4.0/)

